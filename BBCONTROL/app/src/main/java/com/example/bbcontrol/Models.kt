package com.example.bbcontrol

data class DevAlcoholicDrink(val name: String,
                             val description: String,
                             val volume: String,
                             val image : String,
                             val priceGlass: Int,
                             val priceJar: Int,
                             val pricePint: Int,
                             val priceTower: Int) {

}

data class DevNonAlcoholicDrink(val name: String,
                             val image: String,
                             val price: Int) {

}

data class DevFoodPlates(val name: String,
                         val description: String,
                         val category: String,
                         val price: Int){

}