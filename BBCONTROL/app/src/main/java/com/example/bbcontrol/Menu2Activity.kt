package com.example.bbcontrol

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.cardview.widget.CardView
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_menu2.*
import androidx.core.net.toUri as toUri1

class Menu2Activity : AppCompatActivity() {
    var drinks = arrayListOf<DevNonAlcoholicDrink>()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu2)
        transformdata()

        btn1.setOnClickListener {
            finish()
        }
    }
    fun transformdata() {
        val path = "Drinks/A1AX1eCQDVMq9uRSMnGe/Non-alcoholic Drinks"

        db.collection(path)
            .get()
            .addOnCompleteListener() { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    for (document in task.result?.documents!!) {
                        //println(document.data.toString())
                        var  str = ""
                        var datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str = str.plus(dat.toString()).plus(" ,")

                        }
                        //println(str)
                        var part = str.split(" ,")
                        var drink = DevNonAlcoholicDrink(part[2],part[0],part[1].toInt())
                        //println(drink)
                        drinks.add(drink)
                    }
                    Log.d("DB","Se OBTIENE TODA LA INFORMACION")
                    cargar()
                }
            }
    }
    fun cargar(){

        var llDrinkNon = findViewById(R.id.llDrinksNon) as LinearLayout

        var lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        for(drink in drinks) {
            var card = CardView(this)
            card.layoutParams = lp
            card.elevation = 10f
            card.useCompatPadding = true

            val img1 = ImageView(this)
            img1.layoutParams = LinearLayout.LayoutParams(260,260)
            //val uri = Uri.parse(drink.image)
            //img1.setImageURI(uri)
            Picasso.with(this).load(drink.image).into(img1);


            var txt1 = TextView(this)
            txt1.layoutParams = lp
            txt1.text = drink.name

            var txt2 = TextView(this)
            txt2.layoutParams = lp
            txt2.text = drink.price.toString()
            txt2.textSize = 20f

            var llayout = LinearLayout(this)
            llayout.layoutParams = lp
            llayout.orientation = LinearLayout.HORIZONTAL

            llayout.addView(img1)
            llayout.addView(txt1)
            llayout.addView(txt2)

            card.addView(llayout)

            llDrinkNon.addView(card)
        }
    }
}
