package com.example.bbcontrol

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface ReservaDao {

    @Query("SELECT * from reserva_table ORDER BY fecha ASC")
    fun getReservas(): LiveData<List<Reserva>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(reserva: Reserva)

    @Query("DELETE FROM reserva_table")
    suspend fun deleteAll()

    @Delete(entity = Reserva::class)
    fun delete(vararg id: Reserva)

    @Update(entity = Reserva::class)
    fun update(reserva: Reserva);
}