package com.example.bbcontrol

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.sql.Types.NULL

// Annotates class to be a Room Database with a table (entity) of the Reserva class
@Database(entities = arrayOf(Reserva::class), version = 1, exportSchema = false)
public abstract class ReservaRoomDatabase : RoomDatabase() {

    abstract fun reservaDao(): ReservaDao


    private class ReservaDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch {
                    var wordDao = database.reservaDao()

                    // Delete all content here.
                    wordDao.deleteAll()

                    // Add sample words.
                    var word = Reserva(NULL, 20, 5 , "04/03/2020",
                        "10:00", "12:00", 3)
                    wordDao.insert(word)
                    word = Reserva( NULL, 20, 2,  "04/05/2020",
                        "19:00", "22:00", 5)
                    wordDao.insert(word)
                }
            }
        }
    }


    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: ReservaRoomDatabase? = null

        fun getDatabase(context: Context,
                        scope: CoroutineScope
        ): ReservaRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ReservaRoomDatabase::class.java,
                    "word_database"
                )
                    .addCallback(ReservaDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}