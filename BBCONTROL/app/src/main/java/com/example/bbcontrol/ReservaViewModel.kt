package com.example.bbcontrol

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReservaViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ReservaRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allReservas: LiveData<List<Reserva>>

    init {
        val reservasDao = ReservaRoomDatabase.getDatabase(application, viewModelScope).reservaDao()
        repository = ReservaRepository(reservasDao)
        allReservas = repository.allReservas
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(reserva: Reserva) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(reserva)
    }

    /**
     * Launching a new coroutine to update the data in a non-blocking way
     */
    fun update(reserva: Reserva) = viewModelScope.launch(Dispatchers.IO) {
        repository.update(reserva)
    }

    /**
     * Launching a new coroutine to delete the data in a non-blocking way
     */
    fun delete(reserva: Reserva) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(reserva)
    }
}