package com.example.bbcontrol

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_pay.*
import java.util.concurrent.TimeUnit

class PayActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay)

        val objetoIntent: Intent = intent
        val tower: Int = objetoIntent.getIntExtra("tower", 0)
        val jar: Int = objetoIntent.getIntExtra("jar", 0)
        val pint: Int = objetoIntent.getIntExtra("pint", 0)
        val glass: Int = objetoIntent.getIntExtra("glass", 0)

        val valor: Int = tower*59900 + jar*34900 + pint*11900 + glass*7900
        textView2.setText("$valor")

        textView3.setText("${valor*0.10}")

        textView6.setText("${valor*(1+0.10)}")

        btnimg1.setOnClickListener(){
            var online = ConnectionSingleton.isOnline(this.applicationContext)
            if(online){
                Toast.makeText(this, "Order place successfully.", Toast.LENGTH_LONG).show()
            }
            else{
                Toast.makeText(this, "Couldn't place order. \n Your order will be send we the connections is reestablished", Toast.LENGTH_LONG).show()
                Handler().postDelayed(
                    {
                        // This method will be executed once the timer is over
                        while(!online){
                            online = ConnectionSingleton.isOnline(this.applicationContext)
                        }
                        Toast.makeText(this, "Order place successfully.", Toast.LENGTH_LONG).show()
                    },
                    2000 // value in milliseconds
                )




            }


        }
    }
}
