package com.example.bbcontrol

import android.content.Context
import android.net.ConnectivityManager
import android.util.Log
import androidx.core.content.ContextCompat.getSystemService


object ConnectionSingleton{




    init {
        Log.d("Singleton", "Initialization of Conection Sigleton")
    }
    fun isOnline(context: Context): Boolean {
        val connMgr =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        println(connMgr)
        val networkInfo = connMgr!!.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }


    }


