package com.example.bbcontrol

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_registerctivity.*
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*

class Registerctivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    // Access a Cloud Firestore instance from your Activity



    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registerctivity)

        auth = FirebaseAuth.getInstance()
        val db = FirebaseFirestore.getInstance()


        btnRegister.setOnClickListener {
            var online = ConnectionSingleton.isOnline(this.applicationContext)
            if(online){
                var time = LocalTime.now().toString()
                var tot = etDate.text.toString().plus(" ").plus(time).plus(" UTC-5")

                auth.createUserWithEmailAndPassword(etMail.text.toString(), etPassword.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success")
                            val user = auth.currentUser

                            val creat = hashMapOf(
                                "birthDate" to tot,
                                "email" to etMail.text.toString(),
                                "fullName" to etName.text.toString(),
                                "phoneNumber" to etPhone.text.toString()
                            )

                            db.collection("Customers")
                                .add(creat)
                                .addOnSuccessListener { documentReference ->
                                    val usuario = db.collection("Customers").document(documentReference.id)
                                    usuario
                                        .update("id", documentReference.id)
                                        .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully updated!") }
                                        .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }
                                    Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")

                                    val intent = Intent(this, MainActivity::class.java)
                                    startActivity(intent)
                                }
                                .addOnFailureListener { e ->
                                    Log.w(TAG, "Error adding document", e)
                                }




                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        }

                        // ...
                    }
            }
            else{
                Toast.makeText(applicationContext,"No internet connection to any Network",Toast.LENGTH_SHORT).show();
            }



        }


    }
    companion object{
        private const val TAG = "EmailPasswordSIGNUP"
    }
}
