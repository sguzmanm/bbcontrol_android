package com.example.bbcontrol

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView

class FoodActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food)

        val ll_main = findViewById(R.id.prueba) as LinearLayout

        val txtDinamico = TextView(this)
        txtDinamico.textSize = 20f
        txtDinamico.text = "Holiwi"

        ll_main.addView((txtDinamico))
    }
}
