package com.example.bbcontrol

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_order.*

class OrderActivity : AppCompatActivity() {

    var foods = arrayListOf<DevFoodPlates>()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)
        transformdata()

        fat1.setOnClickListener {
            val intent = Intent(this, OrderDetailActivity::class.java)
            intent.putExtra("order", txt1.text.toString())
            startActivity(intent)
        }

        fat2.setOnClickListener {
            val intent = Intent(this, OrderDetailActivity::class.java)
            intent.putExtra("order", txt2.text.toString())
            startActivity(intent)
        }

        fat3.setOnClickListener {
            val intent = Intent(this, OrderDetailActivity::class.java)
            intent.putExtra("order", txt3.text.toString())
            startActivity(intent)
        }

        fat4.setOnClickListener {
            val intent = Intent(this, OrderDetailActivity::class.java)
            intent.putExtra("order", txt4.text.toString())
            startActivity(intent)
        }

        fat5.setOnClickListener {
            val intent = Intent(this, OrderDetailActivity::class.java)
            intent.putExtra("order", txt5.text.toString())
            startActivity(intent)
        }
    }

    fun transformdata(){
        val path = "FoodPlates"

        db.collection(path)
            .get()
            .addOnCompleteListener() { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    for (document in task.result?.documents!!) {
                        //println(document.data.toString())
                        var  str = ""
                        var datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str = str.plus(dat.toString()).plus(" |")

                        }
                        //println(str)
                        var part = str.split(" |")
                        var food = DevFoodPlates(part[1],part[2],part[3],part[0].toInt())
                        //println(food)
                        foods.add(food)

                    }
                    Log.d("DB","Se OBTIENE TODA LA INFORMACION")
                }


            }

    }
}
