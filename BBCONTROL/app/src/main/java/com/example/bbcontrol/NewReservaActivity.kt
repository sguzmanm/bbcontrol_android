package com.example.bbcontrol

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class NewReservaActivity : AppCompatActivity() {

    private lateinit var editDateView: EditText
    private lateinit var editStartView: EditText
    private lateinit var editEndView: EditText
    private lateinit var editPeopleView: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_reserva)
        editDateView = findViewById(R.id.edit_date)
        editStartView = findViewById(R.id.edit_start)
        editEndView = findViewById(R.id.edit_end)
        editPeopleView = findViewById(R.id.edit_people)

        val button = findViewById<Button>(R.id.button_save)
        button.setOnClickListener {
            var online = ConnectionSingleton.isOnline(this.applicationContext)
            if(online){
                val replyIntent = Intent()
                if (TextUtils.isEmpty(editDateView.text)  || TextUtils.isEmpty(editStartView.text) ||
                    TextUtils.isEmpty(editEndView.text) || TextUtils.isEmpty(editPeopleView.text)) {
                    setResult(Activity.RESULT_CANCELED, replyIntent)
                } else {
                    val date = editDateView.text.toString()
                    val start = editStartView.text.toString()
                    val end = editEndView.text.toString()
                    val people =  editPeopleView.text.toString()

                    replyIntent.putExtra("date", date)
                    replyIntent.putExtra("start", start)
                    replyIntent.putExtra("end", end)
                    replyIntent.putExtra("people", people)
                    setResult(Activity.RESULT_OK, replyIntent)
                }
                finish()
            }
            else {
                Toast.makeText(applicationContext,"No internet connection to any Network", Toast.LENGTH_SHORT).show();
            }

        }
    }

}
