package com.example.bbcontrol

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ReservaListAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<ReservaListAdapter.WordViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var reservas = emptyList<Reserva>() // Cached copy of words

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val wordItemView: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
        return WordViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = reservas[position]
        holder.wordItemView.text = "Table: " + current.idMesa.toString() + " - Date: " + current.fecha +
                "\nStart time: " + current.horaInicio + "\nEnd time: " + current.horaFin +
                "\nNum people: " + current.numPersonas

    }

    internal fun setWords(reservas: List<Reserva>) {
        this.reservas = reservas
        notifyDataSetChanged()
    }

    override fun getItemCount() = reservas.size
}