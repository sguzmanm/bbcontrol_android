package com.example.bbcontrol

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_table.*
import java.sql.Types.NULL

class TableActivity : AppCompatActivity() {

    private val newReservaActivityRequestCode = 1
    private lateinit var reservaViewModel: ReservaViewModel
    private var numTable = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_table)

        reservaViewModel = ViewModelProvider(this).get(ReservaViewModel::class.java)

        buttonTable.setOnClickListener{
            val intent = Intent(this, NewReservaActivity::class.java)
            startActivityForResult(intent, newReservaActivityRequestCode)
        }

        historial.setOnClickListener{
            val intent = Intent(this, ReservaActivity::class.java)
            startActivity(intent)
        }

        imageView10.setOnClickListener {
            imageView10.setColorFilter(Color.argb(255, 0, 200, 0))
            imageView11.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView12.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView13.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView16.setColorFilter(Color.argb(0, 0, 0, 0))
            numTable = 1
            buttonTable.isEnabled = true
        }

        imageView11.setOnClickListener {
            imageView11.setColorFilter(Color.argb(255, 0, 200, 0))
            imageView10.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView12.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView13.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView16.setColorFilter(Color.argb(0, 0, 0, 0))
            numTable = 2
            buttonTable.isEnabled = true
        }

        imageView12.setOnClickListener {
            imageView12.setColorFilter(Color.argb(255, 0, 200, 0))
            imageView11.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView10.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView13.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView16.setColorFilter(Color.argb(0, 0, 0, 0))
            numTable = 3
            buttonTable.isEnabled = true
        }

        imageView13.setOnClickListener {
            imageView13.setColorFilter(Color.argb(255, 0, 200, 0))
            imageView11.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView12.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView10.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView16.setColorFilter(Color.argb(0, 0, 0, 0))
            numTable = 4
            buttonTable.isEnabled = true
        }

        imageView16.setOnClickListener {
            imageView16.setColorFilter(Color.argb(255, 0, 200, 0))
            imageView11.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView12.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView13.setColorFilter(Color.argb(0, 0, 0, 0))
            imageView10.setColorFilter(Color.argb(0, 0, 0, 0))
            numTable = 5
            buttonTable.isEnabled = true
        }

        switch1.visibility = View.VISIBLE
        imageView16.visibility = View.VISIBLE
        imageView10.visibility = View.VISIBLE
        imageView13.visibility = View.VISIBLE
        imageView12.visibility = View.VISIBLE
        imageView11.visibility = View.VISIBLE
        buttonTable.isEnabled = false

        var visibilidad: Int = 0
        switch1.setOnClickListener{
            if (visibilidad == 0) {
                /*
                imageView16.visibility = View.VISIBLE
                imageView10.visibility = View.VISIBLE
                imageView13.visibility = View.INVISIBLE
                imageView12.visibility = View.INVISIBLE
                imageView11.visibility = View.INVISIBLE
                */
                imageView16.setColorFilter(Color.argb(255, 0, 0, 200))
                imageView10.setColorFilter(Color.argb(255, 0, 0, 200))
                imageView11.setColorFilter(Color.argb(255, 200, 0, 0))
                imageView12.setColorFilter(Color.argb(255, 200, 0, 0))
                imageView13.setColorFilter(Color.argb(255, 200, 0, 0))
                visibilidad = 1
            }
            else{
                /*
                imageView16.visibility = View.INVISIBLE
                imageView10.visibility = View.INVISIBLE
                imageView13.visibility = View.VISIBLE
                imageView12.visibility = View.VISIBLE
                imageView11.visibility = View.VISIBLE
                 */
                imageView16.setColorFilter(Color.argb(0, 0, 0, 0))
                imageView10.setColorFilter(Color.argb(0, 0, 0, 0))
                imageView11.setColorFilter(Color.argb(0, 0, 0, 0))
                imageView12.setColorFilter(Color.argb(0, 0, 0, 0))
                imageView13.setColorFilter(Color.argb(0, 0, 0, 0))
                visibilidad = 0
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newReservaActivityRequestCode && resultCode == Activity.RESULT_OK) {
            val date = data?.getStringExtra("date").toString()
            val start = data?.getStringExtra("start").toString()
            val end = data?.getStringExtra("end").toString()
            val people = data?.getStringExtra("people").toString().toInt()

            val reserva = Reserva( NULL, 1, numTable, date, start, end, people)

            reservaViewModel.insert(reserva)
        } else {
            Toast.makeText(
                applicationContext,
                R.string.error,
                Toast.LENGTH_LONG).show()
        }
    }
}
