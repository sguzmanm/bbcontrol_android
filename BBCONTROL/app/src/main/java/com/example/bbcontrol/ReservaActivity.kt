package com.example.bbcontrol

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ReservaActivity : AppCompatActivity() {

    private lateinit var reservaViewModel: ReservaViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reserva)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = ReservaListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        reservaViewModel = ViewModelProvider(this).get(ReservaViewModel::class.java)

        reservaViewModel.allReservas.observe(this, Observer { reservas ->
            // Update the cached copy of the words in the adapter.
            reservas?.let { adapter.setWords(it) }
        })
    }
}
