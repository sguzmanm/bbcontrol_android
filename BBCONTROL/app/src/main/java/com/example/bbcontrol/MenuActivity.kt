package com.example.bbcontrol

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {
    var drinks = arrayListOf<DevAlcoholicDrink>()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        cargar { transformdata() }

        btn2.setOnClickListener {
            val intent = Intent(this, Menu2Activity::class.java)
            startActivity(intent)
        }
    }

   fun transformdata() {
        val path = "Drinks/A1AX1eCQDVMq9uRSMnGe/Alcoholic Drinks/H6vBaPIidRlPTSFJDyAk/Beers"

        db.collection(path)
            .get()
            .addOnCompleteListener(){task: Task<QuerySnapshot> ->
                if(task.isSuccessful){
                    for ( document in task.result?.documents!!){
                        var  str = ""
                        var datos = Pair(document.id,document.data)
                        //println(datos.second?.values.toString())
                        for (dat in datos.second?.values!!){
                            str = str.plus(dat.toString()).plus(" ,")
                        }

                        var path2 = path.plus("/").plus(datos.first.toString()).plus("/Prices")
                        db.collection(path2)
                            .get()
                            .addOnCompleteListener(){task2: Task<QuerySnapshot> ->
                                if(task.isSuccessful){
                                    for (document2 in task2.result?.documents!!){
                                        var datos2 = Pair(document2.id,document2.data)
                                        //println(datos2.second?.values.toString())

                                        for (dat2 in datos2.second?.values!!){
                                            str = str.plus(dat2.toString()).plus(" ,")
                                        }
                                        // println(str)
                                        var part = str.split(" ,")
                                        //println(part)
                                        var drink = DevAlcoholicDrink(part[2],part[3],part[0],part[1],part[4].toInt(),part[5].toInt(),part[6].toInt(),part[7].toInt())
                                        drinks.add(drink)
                                    }
                                }
                            }
                    }
                    Log.d("DB","Se OBTIENE TODA LA INFORMACION")
                }
            }
    }
    fun cargar(transformdata: () -> Unit) {

        var llDrink = findViewById(R.id.llDrinks) as LinearLayout

        var lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        for(drink in drinks) {
            var card = CardView(this)
            card.layoutParams = lp
            card.elevation = 10f
            card.useCompatPadding = true

            val img1 = ImageView(this)
            img1.layoutParams = LinearLayout.LayoutParams(260,260)
            //val uri = Uri.parse(drink.image)
            //img1.setImageURI(uri)
            Picasso.with(this).load(drink.image).into(img1)

            var txt1 = TextView(this)
            txt1.layoutParams = lp
            txt1.text = drink.name

            var txt2 = TextView(this)
            txt2.layoutParams = lp
            txt2.text = drink.description
            txt2.textSize = 20f

            var llayoutV = LinearLayout(this)
            llayoutV.layoutParams = lp
            llayoutV.orientation = LinearLayout.VERTICAL

            llayoutV.addView(txt1)
            llayoutV.addView(txt2)

            var llayout = LinearLayout(this)
            llayout.layoutParams = lp
            llayout.orientation = LinearLayout.HORIZONTAL

            llayout.addView(img1)
            llayout.addView(llayoutV)

            card.addView(llayout)
            llDrink.addView(card)
        }
    }
}
