package com.example.bbcontrol

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.net.toUri
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_main.*

class CartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        val objetoIntent: Intent = intent
        val beer = objetoIntent.getStringExtra("beer")

        textv1.text = beer

        imgb1.setOnClickListener {
            val intent = Intent(this, PayActivity::class.java)
            intent.putExtra("tower", if (field1.text.toString().length<=0) 0 else field1.text.toString().toInt())
            intent.putExtra("jar", if (field2.text.toString().length<=0) 0 else field2.text.toString().toInt())
            intent.putExtra("pint", if (field3.text.toString().length<=0) 0 else field3.text.toString().toInt())
            intent.putExtra("glass", if (field4.text.toString().length<=0) 0 else field4.text.toString().toInt())
            startActivity(intent)
        }
    }
}
